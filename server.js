import express from 'express'
import mcache from 'memory-cache'
import logger from 'heroku-logger'
import request from 'request'
//import https from 'https'

const app = express()

/** CONFIG **/
const port = process.env.PORT || 8001

const headers = {
    'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36',
    'Cookie': '_member_location=40.73085415675699%2C-73.93559375344463; _member_role=1; _member_token=eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJkYXZlYW1hdG9AbGl2ZS5jb20iLCJleHAiOjE1NDUzOTgzNDZ9.BNPqfPKmX4L0AF74NnVuZCDOPThfZlvWOpJRU0TEdEM; _member_username=daveamato@live.com'
};

const options = {
    url: 'http://en.wikipedia.org/',
    headers: headers
};

const callback = function(error, response, body) {
    if (!error && response.statusCode == 200) {
        console.log(body);
    }
}

const requestCache = (duration) => {
  return (req, res, next) => {
    let key = `__express__${req.originalUrl}` || req.url
    let cachedBody = mcache.get(key)
    if (cachedBody) {
      res.send(cachedBody)
    } else {
      res.sendResponse = res.send
      res.send = (body) => {
        mcache.put(key, body, duration)
        res.sendResponse(body)
      }
      next()
    }
  }
}

/** ROUTES **/
/* home */
app.get('/', (req, res) => {
	if (req.params.dlUrl === '' || req.params.dlUrl === 'favicon.ico') { return } 
	const text = '<h1>locust-server</h1><p>This is just a demonstration, not for public us.</p>';
	res.send(text);
});

app.get('/test', (req, res) => {
	const s = request(options, callback);
	req.pipe(s);
	s.pipe(res);
})

app.get('/play/*', (req, res) => {

  let ch = req.params[0];
  if (isNaN(ch)) { return 'Failed'; }
  //logger.info('getting', { url: path })
  console.log('playing: ', ch);
  
  const opts = {
  	url: 'https://www.locast.org/wp/wp-admin/admin-ajax.php?station_id=' + ch + '&lat=40.730610&action=get_station&lon=-73.935242',
  	headers: headers
  }
  
  request(opts, function(err, resp, json) {
  	json = JSON.parse(json);
  	const u = json['streamUrl'];
  	console.log(u);
  	res.redirect(u);
  	
  });
  //req.pipe(x);
  
  /* const j = JSON.parse(body)
  console.log(JSON.stringify(j));
  req.pipe(r);
  r.pipe(res); */

 });
  /*
  youtubedl.getInfo(path, (err, info) => {
    if (err) {
      res.send({ status: false, error: err })
    }*/
    //logger.info('resolved', { 'url': info.url })
    
    
    
    //res.setHeader('Content-Type', 'application/x-mpegURL');
    //res.attachment(info._filename);
    //https.get(path).pipe(res);
    
    /*
    let stat = fs.statSync(info.url)
    let fileSize = stat.size
    let range = req.headers.range
    if (range) {
      let parts = range.replace(/bytes=/, "").split("-")
      let start = parseInt(parts[0], 10)
      let end = parts[1] 
        ? parseInt(parts[1], 10)
        : fileSize-1
      let chunksize = (end-start)+1
      let file = fs.createReadStream(path, {start, end})
      let head = {
        'Content-Range': `bytes ${start}-${end}/${fileSize}`,
        'Accept-Ranges': 'bytes',
        'Content-Length': chunksize,
        'Content-Type': 'video/mp4',
      }
      res.writeHead(206, head);
      file.pipe(res);
    } else {
      let head = {
        'Content-Length': fileSize,
        'Content-Type': 'video/mp4',
      }
      res.writeHead(200, head)
      fs.createReadStream(path).pipe(res)
    }
    
    */
    
    
    
    /*
    obJ = {
      success: true,
      data: {
        id: info.id||'None',
        title: info.title||'None',
        stream: info.url||'None',
        thumbnail: info.thumbnail||'None'
      }
    }
    */
//});

/** LISTEN **/
app.listen(port, function () {
     console.log("Running API on port " + port);
     logger.info('started', { success: true })
});
